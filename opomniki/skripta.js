window.addEventListener('load', function() {
	//stran nalozena
		function izvediPrijavo() {
			var ime = document.querySelector("#uporabnisko_ime").value;
			document.querySelector("#uporabnik").innerHTML = ime;
			document.querySelector(".pokrivalo").style.visibility = "hidden";
		}
		
		document.querySelector("#prijavniGumb").addEventListener("click", izvediPrijavo);
		
		function dodajOpomnik() {
			var naziv = document.querySelector("#naziv_opomnika").value;
			var cas = document.querySelector("#cas_opomnika").value;
			
			document.querySelector("#naziv_opomnika").value = "";
			document.querySelector("#cas_opomnika").value = "";
			
			var opomniki = document.querySelector("#opomniki");
			opomniki.innerHTML += " \
				<div class='opomnik' class = 'senca rob'> \
					<div class='naziv_opomnika'>NAZIV_OPOMNIKA</div> \
					<div class='cas_opomnika'> Opomnik čez <span>CAS_OPOMNIKA</span> sekund.</div> \
				</div>"
			
		}
		
		document.querySelector("#dodajGumb").addEventListener("click", dodajOpomnik);
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			
			if (cas == 0) {
				var naziv = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev " + naziv + " je potekla!")
				document.querySelector("#opomniki").removeChild(opomnik);
			}
			else {
				casovnik.innerHTML = cas--;
			}

		}
	}
	setInterval(posodobiOpomnike, 1000);
	
});